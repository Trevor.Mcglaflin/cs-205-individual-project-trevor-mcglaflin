from datetime import datetime


class Agency:
    def __init__(self, name, phone, email, address, zip_code, realtors):
        self.__name = name
        self.__phone = phone
        self.__email = email
        self.__zip_code = zip_code
        self.__address = address
        self.__realtors = realtors

    # getter methods
    def get_name(self):
        return self.__name

    def get_phone(self):
        return self.__phone

    def get_email(self):
        return self.__email

    def get_address(self):
        return self.__address

    def get_zip_code(self):
        return self.__zip_code

    def get_realtors(self):
        return self.__realtors

    # setter methods
    def set_name(self, name):
        self.__name = name

    def set_phone(self, phone):
        self.__phone = phone

    def set_email(self, email):
        self.__email = email

    def set_address(self, address):
        self.__address = address

    def set_zip_code(self, zip_code):
        self.__zip_code = zip_code

    def set_realtors(self, realtors):
        self.__realtors = realtors

    # other public methods
    def get_houses(self):
        houses = []
        for realtor in self.__realtors:
            for house in realtor.get_houses():
                houses.append(house)
        return houses

    def get_houses_for_sale(self):
        houses = self.get_houses()
        houses_for_sale = []
        for house in houses:
            if house.get_date_sold() is None:
                houses_for_sale.append(house)
        return houses_for_sale

    def get_houses_sold(self, since_date=None, to_date=None):
        if since_date is None:
            since_date = "1900-01-01"
        if to_date is None:
            today = datetime.now()
            to_date = today.strftime("%Y-%m-%d")
        houses_sold = []
        for realtor in self.__realtors:
            houses_sold_by_realtor = realtor.get_houses_sold(since_date, to_date)
            for house in houses_sold_by_realtor:
                houses_sold.append(house)
        return houses_sold

    def get_revenue(self, since_date=None, to_date=None):
        houses_sold = self.get_houses_sold(since_date, to_date)
        revenue = 0
        for house in houses_sold:
            revenue += house.get_market_price()
        return revenue











