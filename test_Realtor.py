import unittest
from Realtor import Realtor
from House import House

class TestRealtor(unittest.TestCase):

    def setUp(self):
        # house objects are needed because realtors have houses assigned to them
        self.house1 = House(500000, "406 North Street", "05401", 4, 1.5, 1300, "2021-02-22", None)
        self.house2 = House(400000, "58 Buell Street", "05401", 3, 1, 1250, "2020-07-07", "2021-03-12")
        self.house3 = House(-5, "1 Main Street", "-05449", -3, -1, -1250, "2021-03-12", "2020-07-07")
        self.house4 = House(300000, "13 Church Street", "05401", 3, 3, 1000, "2020-02-02", "2020-04-06")
        self.house5 = House(200000, "1 Maple Street", "05401", 1, 1, 1000, "2020-05-03", None)

        # create realtor objects
        self.realtor1 = Realtor("Trevor McGlaflin", "802-324-7275", "tmcglaflin@realtor.com", [self.house1, self.house2])
        self.realtor2 = Realtor("Jason Hibbeler", "802-xxx-xxxx", "jhib@realtor.com")
        self.realtor3 = Realtor("Jim Eddy", "800-xxx-xxxx", "jeddy@realtor.com", [self.house2, self.house4])
        self.realtor4 = Realtor("Jackie Horton", "802-xxx-xxxx", "jhorton@realtor.com", [self.house1, self.house2, self.house3, self.house4, self.house5])



    def test_name(self):
        self.assertEqual(self.realtor1.get_name(), "Trevor McGlaflin")
        self.realtor1.set_name("Patrick McGlaflin")
        self.assertEqual(self.realtor1.get_name(), "Patrick McGlaflin")

    def test_phone(self):
        self.assertEqual(self.realtor1.get_phone(), "802-324-7275")
        self.realtor1.set_phone("802-425-7625")
        self.assertEqual(self.realtor1.get_phone(), "802-425-7625")

    def test_email(self):
        self.assertEqual(self.realtor1.get_email(), "tmcglaflin@realtor.com")
        self.realtor1.set_email("tmcglaflin@uvm.edu")
        self.assertEqual(self.realtor1.get_email(), "tmcglaflin@uvm.edu")

    def test_houses(self):
        self.assertEqual(self.realtor1.get_houses(), [self.house1, self.house2])
        self.assertEqual(self.realtor2.get_houses(), [])

        self.realtor1.set_houses([self.house1])

        self.assertEqual(self.realtor1.get_houses(), [self.house1])

    def test_houses_sold(self):
        # test for all time periods
        self.assertEqual(self.realtor1.get_houses_sold(), [self.house2])
        self.assertEqual(self.realtor3.get_houses_sold(), [self.house2, self.house4])

        # test when start date is specified (end date will be current date)
        self.assertEqual(self.realtor3.get_houses_sold("2021-01-01"), [self.house2])

        # test when start and end date are both given
        self.assertEqual(self.realtor3.get_houses_sold("2020-01-01", "2021-01-01"), [self.house4])

    def test_get_revenue(self):
        # test for all time periods
        self.assertEqual(self.realtor1.get_revenue(), 400000)
        self.assertEqual(self.realtor3.get_revenue(), 700000)

        # test when start date is specified (end date will be current date)
        self.assertEqual(self.realtor3.get_revenue("2021-01-01"), 400000)

        # test when start and end date are both given
        self.assertEqual(self.realtor3.get_revenue("2020-01-01", "2021-01-01"), 300000)

    def test_houses_to_sell(self):
        # test when realtor has one or more houses to sell
        self.assertEqual(self.realtor1.get_houses_to_sell(), [self.house1])
        self.assertEqual(self.realtor4.get_houses_to_sell(), [self.house1, self.house5])

        # test when all houses have been sold
        self.assertEqual(self.realtor3.get_houses_to_sell(), [])

    def test_average_sell_time(self):
        # test when no houses have been sold
        self.assertEqual(self.realtor2.get_average_sell_time(), -1)
        self.assertEqual(self.realtor4.get_average_sell_time("2020-07-08", "2020-07-09"), -1)

        # test when houses have been sold (and when since date and to date are given)
        self.assertEqual(self.realtor4.get_average_sell_time(), (self.house2.length_listed() + self.house3.length_listed() + self.house4.length_listed())/3)
        self.assertEqual(self.realtor4.get_average_sell_time("2020-07-01"), (self.house2.length_listed() + self.house3.length_listed())/2)
        self.assertEqual(self.realtor4.get_average_sell_time("2020-04-06", "2020-07-07"), (self.house3.length_listed() + self.house4.length_listed())/2)

























