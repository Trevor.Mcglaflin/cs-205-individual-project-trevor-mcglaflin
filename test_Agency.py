import unittest
from Agency import Agency
from Realtor import Realtor
from House import House


class TestAgency(unittest.TestCase):
    def setUp(self):
        # house objects are needed because realtors have houses assigned to them
        self.house1 = House(500000, "406 North Street", "05401", 4, 1.5, 1300, "2021-02-22", None)
        self.house2 = House(400000, "58 Buell Street", "05401", 3, 1, 1250, "2020-07-07", "2021-03-12")
        self.house3 = House(200000, "1 Main Street", "-05449", -3, -1, -1250, "2021-03-12", "2020-07-07")
        self.house4 = House(325000, "1 South Union Street", "05401", 3, 1, 1000, "2020-05-05", None)

        # create realtor objects
        self.realtor1 = Realtor("Trevor McGlaflin", "802-324-7275", "tmcglaflin@realtor.com", [self.house1, self.house2])
        self.realtor2 = Realtor("Jason Hibbeler", "802-xxx-xxxx", "jhib@realtor.com", [self.house3, self.house4])

        # create an agency object
        # name, phone, email, address, realtors, houses)
        self.agency = Agency("McGlaflin & Co", "802-425-7625", "mcglaflin@realestate.com", "5 Night Run Road", "05445", [self.realtor1, self.realtor2])

    def test_name(self):
        self.assertEqual(self.agency.get_name(), "McGlaflin & Co")
        self.agency.set_name("McGlaflin & Sons")
        self.assertEqual(self.agency.get_name(), "McGlaflin & Sons")

    def test_phone(self):
        self.assertEqual(self.agency.get_phone(), "802-425-7625")
        self.agency.set_phone("802-324-7275")
        self.assertEqual(self.agency.get_phone(), "802-324-7275")

    def test_email(self):
        self.assertEqual(self.agency.get_email(), "mcglaflin@realestate.com")
        self.agency.set_email("mcglaflin@realtors.net")
        self.assertEqual(self.agency.get_email(), "mcglaflin@realtors.net")

    def test_address(self):
        self.assertEqual(self.agency.get_address(), "5 Night Run Road")
        self.agency.set_address("406 North Street")
        self.assertEqual(self.agency.get_address(), "406 North Street")

    def test_zip_code(self):
        self.assertEqual(self.agency.get_zip_code(), "05445")
        self.agency.set_zip_code("05401")
        self.assertEqual(self.agency.get_zip_code(), "05401")

    def test_realtors(self):
        self.assertEqual(self.agency.get_realtors(), [self.realtor1, self.realtor2])
        self.agency.set_realtors([self.realtor1])
        self.assertEqual(self.agency.get_realtors(), [self.realtor1])

    def test_get_houses(self):
        self.assertEqual(self.agency.get_houses(), [self.house1, self.house2, self.house3, self.house4])

    def test_houses_for_sale(self):
        self.assertEqual(self.agency.get_houses_for_sale(), [self.house1, self.house4])

    def test_get_houses_sold(self):
        self.assertEqual(self.agency.get_houses_sold(), [self.house2, self.house3])
        self.assertEqual(self.agency.get_houses_sold("2021-01-01"), [self.house2])
        self.assertEqual(self.agency.get_houses_sold("2020-07-06", "2020-07-08"), [self.house3])

    def test_get_revenue(self):
        self.assertEqual(self.agency.get_revenue(), 600000)
        self.assertEqual(self.agency.get_revenue("2021-01-01"), 400000)
        self.assertEqual(self.agency.get_revenue("2020-07-06", "2020-07-08"), 200000)


























if __name__ == '__main__':

    unittest.main()