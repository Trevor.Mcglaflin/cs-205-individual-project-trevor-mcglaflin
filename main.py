from Agency import Agency
from House import House
from Realtor import Realtor
import csv

def main():
    realtors = get_realtors_from_file("Realtors.csv")
    houses = get_houses_from_file("Houses.csv", realtors)
    for house in houses:
        print(house.get_market_price(), " " + house.get_address())

def get_realtors_from_file(file_name):
    realtors = []
    with open(file_name) as file:
        reader = csv.reader(file, delimiter=',')
        line_count = 0
        for row in reader:
            if line_count == 0:
                line_count += 1
            else:
                realtors.append(Realtor(row[0], row[1], row[2]))
                line_count += 1
    return realtors

def get_houses_from_file(file_name, realtors):
    houses = []
    with open(file_name) as file:
        reader = csv.reader(file, delimiter=',')
        line_count = 0
        for row in reader:
            if line_count == 0:
                line_count += 1
            else:
                if row[6] != "":
                    houses.append(House(int(row[0]), row[1], row[2], int(row[3]), float(row[4]), int(row[5]), row[6], row[7]))
                else:
                    houses.append(House(int(row[0]), row[1], row[2], int(row[3]), float(row[4]), int(row[5]), row[6], None))
    return houses





main()
