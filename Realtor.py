from datetime import datetime



class Realtor:
    def __init__(self, name, phone, email, houses = []):
        self.__name = name
        self.__phone = phone
        self.__email = email
        self.__houses = houses

    # getter methods
    def get_name(self):
        return self.__name

    def get_phone(self):
        return self.__phone

    def get_phone(self):
        return self.__phone

    def get_email(self):
        return self.__email

    def get_houses(self):
        return self.__houses

    # setter methods
    def set_name(self, name):
        self.__name = name

    def set_phone(self, phone):
        self.__phone = phone

    def set_email(self, email):
        self.__email = email

    def set_houses(self, houses):
        self.__houses = houses

    # adds a house to a realtors house list
    def add_house(self, house):
        self.__houses.append(house)

    # if no parameters are given it will return all houses the realtor has ever sold
    # if one parameter is given, it will return the houses sold since that date to the current date
    # if two parameters are given, it will return the houses sold from date1 to date2
    def get_houses_sold(self, since_date=None, to_date=None):
        if since_date is None:
            since_date = "1900-01-01"
        if to_date is None:
            today = datetime.now()
            to_date = today.strftime("%Y-%m-%d")

        houses_sold = []
        for house in self.__houses:
            if house.get_date_sold() is not None:
                if since_date <= house.get_date_sold() <= to_date:
                    houses_sold.append(house)
        return houses_sold

    # will return how much revenue the realtor has sold
    def get_revenue(self, since_date=None, to_date=None):
        if since_date is None:
            since_date = "1900-01-01"
        if to_date is None:
            today = datetime.now()
            to_date = today.strftime("%Y-%m-%d")
        # start by getting a list of all houses sold since the given date
        houses_sold_since_date = self.get_houses_sold(since_date, to_date)

        # now sum up all the revenue from those houses
        revenue_sum = 0
        for house in houses_sold_since_date:
            revenue_sum += house.get_market_price()
        return revenue_sum

    def get_houses_to_sell(self):
        houses_to_sell = []
        for house in self.__houses:
            if house.get_date_sold() is None:
                houses_to_sell.append(house)
        return houses_to_sell

    def get_average_sell_time(self, since_date=None, to_date=None):
        if since_date is None:
            since_date = "1900-01-01"
        if to_date is None:
            today = datetime.now()
            to_date = today.strftime("%Y-%m-%d")
        # start by getting a list of all houses sold since the given date
        houses_sold_since_date = self.get_houses_sold(since_date, to_date)

        # sum up average length listed
        acc_sell_time = 0
        for house in houses_sold_since_date:
            acc_sell_time += house.length_listed()

        # if no houses have been sold return -1
        if len(houses_sold_since_date) == 0:
            return -1
        else:
            return acc_sell_time/len(houses_sold_since_date)

