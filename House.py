from datetime import datetime


class House:
    def __init__(self, market_price, address, zip_code, num_bedrooms, num_bathrooms, square_feet, date_listed, date_sold):
        if market_price >= 0:
            self.__market_price = market_price
        else:
            self.__market_price = 0
        self.__address = address
        self.__zip_code = zip_code
        if num_bedrooms >= 0:
            self.__num_bedrooms = num_bedrooms
        else:
            self.__num_bedrooms = 0
        if num_bathrooms >= 0:
            self.__num_bathrooms = num_bathrooms
        else:
            self.__num_bathrooms = 0
        if square_feet >= 0:
            self.__square_feet = square_feet
        else:
            self.__square_feet = 0
        self.__date_listed = date_listed
        self.__date_sold = date_sold

        if date_sold is not None:
            if date_listed > date_sold:
                self.__date_listed = date_sold
            else:
                self.__date_listed = date_listed
        else:
            self.__date_listed = date_listed

    # getter methods
    def get_market_price(self):
        return self.__market_price

    def get_address(self):
        return self.__address

    def get_zip_code(self):
        return self.__zip_code

    def get_num_bedrooms(self):
        return self.__num_bedrooms

    def get_num_bathrooms(self):
        return self.__num_bathrooms

    def get_square_feet(self):
        return self.__square_feet

    def get_date_listed(self):
        return self.__date_listed

    def get_date_sold(self):
        return self.__date_sold

    # setter methods
    def set_market_price(self, market_price):
        if market_price >= 0:
            self.__market_price = market_price
        else:
            self.__market_price = 0

    def set_address(self, address):
        self.__address = address

    def set_zip_code(self, zip_code):
        self.__zip_code = zip_code

    def set_num_bedrooms(self, num_bedrooms):
        if num_bedrooms >= 0:
            self.__num_bedrooms = num_bedrooms
        else:
            self.__num_bedrooms = 0

    def set_num_bathrooms(self, num_bathrooms):
        if num_bathrooms >= 0:
            self.__num_bathrooms = num_bathrooms
        else:
            self.__num_bathrooms = 0

    def set_square_feet(self, square_feet):
        if square_feet >= 0:
            self.__square_feet = square_feet
        else:
            self.__square_feet = 0

    def set_date_listed(self, date_listed):
        if self.__date_sold is not None:
            if date_listed > self.__date_sold:
                self.__date_listed = self.__date_sold
            else:
                self.__date_listed = date_listed
        else:
            self.__date_listed = date_listed

    def set_date_sold(self, date_sold):
        if date_sold is not None:
            if date_sold < self.__date_listed:
                self.__date_sold = self.__date_listed
            else:
                self.__date_sold = date_sold
        else:
            self.__date_sold = date_sold

    # call when you sell a house, if no parameter is given, it will assume that you sold
    # the house on the current day. If the given sell date is earlier than the date_listed
    # the sell_date will equal the date listed
    def sell(self, date_sold=None):
        if date_sold is None:
            today = datetime.today()
            formatted_today = today.strftime("%Y-%m-%d")
            self.__date_sold = formatted_today
        else:
            if date_sold > self.__date_listed:

                self.__date_sold = date_sold
            else:
                self.__date_sold = self.__date_listed

    # returns number of days the house was listed (if sold already),
    # if already sold, it will return the number of days passed since date listed
    def length_listed(self):
        start = datetime.strptime(self.__date_listed, "%Y-%m-%d")
        if self.__date_sold is None:
            end = datetime.today()
        else:
            end = datetime.strptime(self.__date_sold, "%Y-%m-%d")
        return (end-start).days






