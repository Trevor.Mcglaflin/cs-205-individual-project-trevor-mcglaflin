import unittest
from House import House
from datetime import datetime


class TestHouse(unittest.TestCase):
    # called before each test
    def setUp(self):
        self.house1 = House(500000, "406 North Street", "05401", 4, 1.5, 1300, "2021-02-22", None)
        self.house2 = House(400000, "58 Buell Street", "05401", 3, 1, 1250, "2020-07-07", "2021-03-12")
        self.house3 = House(-5, "1 Main Street", "-05449", -3, -1, -1250, "2021-03-12", "2020-07-07")
        self.house4 = House(100000, "1 Pearl Street", "05445", 1, 1, 900, "2020-02-22", "2020-02-25")

    # if negative value is given in constructor or set_market_price, it should set it to 0
    def test_market_price(self):
        self.assertEqual(self.house1.get_market_price(), 500000)
        self.assertEqual(self.house3.get_market_price(), 0)

        self.house1.set_market_price(450000)
        self.house2.set_market_price(-50)

        self.assertEqual(self.house1.get_market_price(), 450000)
        self.assertEqual(self.house3.get_market_price(), 0)

    def test_address(self):
        self.assertEqual(self.house1.get_address(), "406 North Street")
        self.house1.set_address("44 Pearl Street")
        self.assertEqual(self.house1.get_address(), "44 Pearl Street")

    def test_zip_code(self):
        self.assertEqual(self.house1.get_zip_code(), "05401")
        self.house1.set_zip_code("05445")
        self.assertEqual(self.house1.get_zip_code(), "05445")

    # if negative value is given in constructor or set_num_bedrooms, it should set it to 0
    def test_num_bedrooms(self):
        self.assertEqual(self.house1.get_num_bedrooms(), 4)
        self.assertEqual(self.house3.get_num_bedrooms(), 0)

        self.house1.set_num_bedrooms(5)
        self.house3.set_num_bedrooms(-10)

        self.assertEqual(self.house1.get_num_bedrooms(), 5)
        self.assertEqual(self.house3.get_num_bedrooms(), 0)

    # if negative value is given in constructor or set_num_bathrooms, it should set it to 0
    def test_num_bathrooms(self):
        self.assertEqual(self.house1.get_num_bathrooms(), 1.5)
        self.assertEqual(self.house3.get_num_bathrooms(), 0)

        self.house1.set_num_bathrooms(2)
        self.house3.set_num_bathrooms(-10)

        self.assertEqual(self.house1.get_num_bathrooms(), 2)
        self.assertEqual(self.house3.get_num_bathrooms(), 0)

    # if negative value is given in constructor or set_square_feet, it should set it to 0
    def test_square_feet(self):
        self.assertEqual(self.house1.get_square_feet(), 1300)
        self.assertEqual(self.house3.get_square_feet(), 0)

        self.house1.set_square_feet(1500)
        self.house3.set_square_feet(-10)

        self.assertEqual(self.house1.get_square_feet(), 1500)
        self.assertEqual(self.house3.get_square_feet(), 0)

    # if the date listed given in the constructor or in the set_date_listed method
    # is after the sell date, we will set it to the sell date
    def test_date_listed(self):
        self.assertEqual(self.house1.get_date_listed(), "2021-02-22")
        self.assertEqual(self.house2.get_date_listed(), "2020-07-07")
        self.assertEqual(self.house3.get_date_listed(), "2020-07-07")

        self.house1.set_date_listed("2021-01-22")
        self.house2.set_date_listed("2021-03-14")

        self.assertEqual(self.house1.get_date_listed(), "2021-01-22")
        self.assertEqual(self.house2.get_date_listed(), "2021-03-12")

    # if the date sold given in the get_date_sold method is before the date_listed
    # we will set it to the date_listed
    def test_date_sold(self):
        self.assertIsNone(self.house1.get_date_sold())
        self.assertEqual(self.house2.get_date_sold(), "2021-03-12")
        self.assertEqual(self.house3.get_date_sold(), "2020-07-07")

        self.house1.set_date_sold("2021-03-01")
        self.house2.set_date_sold("2020-01-12")

        self.assertEqual(self.house1.get_date_sold(), "2021-03-01")
        self.assertEqual(self.house2.get_date_sold(), "2020-07-07")

    def test_sell(self):
        today = datetime.today()
        formatted_today = today.strftime("%Y-%m-%d")

        # test with no parameter
        self.house1.sell()
        self.assertEqual(self.house1.get_date_sold(), formatted_today)

        # test with parameter
        self.house1.sell("2021-03-22")
        self.assertEqual(self.house1.get_date_sold(), "2021-03-22")

        # test case where given sell date is before date_listed
        self.house1.sell("2016-03-22")
        self.assertEqual(self.house1.get_date_sold(), "2021-02-22")

    def test_length_listed(self):
        # test case when there is no sell date
        start = datetime.strptime(self.house1.get_date_listed(), "%Y-%m-%d")
        end = datetime.today()
        self.assertEqual(self.house1.length_listed(), (end-start).days)

        # test case when there is a sell date
        self.assertEqual(self.house4.length_listed(), 3)



















